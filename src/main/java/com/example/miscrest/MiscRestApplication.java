package com.example.miscrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiscRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(MiscRestApplication.class, args);
    }

}
