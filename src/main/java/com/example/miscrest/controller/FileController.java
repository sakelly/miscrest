package com.example.miscrest.controller;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.example.miscrest.entity.Image;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/file")
@CrossOrigin (origins = "http://localhost:8081")
public class FileController {
    @GetMapping("upload")
    public String noGetOnFileUpload() {
        return "get is not really appropriate for uploading a file";
    }
    @PostMapping("upload")
    public Image fileUpload(@RequestParam("file") MultipartFile file) {
        System.out.println("file.getOriginalFilename() = " + file.getOriginalFilename());
        Image image = new Image();
        try {
            TimeUnit.MILLISECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        image.setId(42+(new Date()).getTime());
        System.out.println(image);
        return image;
    }
}
